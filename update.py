#!/usr/bin/env python3
import pysftp
import os
from dotenv import load_dotenv
import os.path
import subprocess

# Get environment variables
load_dotenv()
USER = os.getenv('SFTP_USER')
PASSWORD = os.getenv('SFTP_PASSWORD')
SERVER = os.getenv('SFTP_SERVER')
PORT = os.getenv('SFTP_PORT') or 22
PLUGINFOLDER = os.getenv('SFTP_FOLDER') or '/plugins'
JARFILE = os.getenv('JARFILE')
cinfo = {'host': SERVER, 'username': USER,
         'password': PASSWORD, 'port': PORT, 'default_path':PLUGINFOLDER}

def getProjectName():
    if os.getenv('PROJECT'):
        return os.getenv('PROJECT')
    DASHINDEX = JARFILE.find('-')
    return JARFILE[0:DASHINDEX]

def getOldJars() :
    with pysftp.Connection(**cinfo) as sftp:
        files = sftp.listdir()
        JARNAME = JARFILE.removesuffix('.jar')
        LIST = list(filter(lambda x: x.startswith(getProjectName()) and x.endswith('.jar'), files))
    return LIST
def deleteOldJars() :
    with pysftp.Connection(**cinfo) as sftp:
        for jar in getOldJars():
            print("Removing "+jar)
            sftp.remove(jar)

def uploadJar() :
    with pysftp.Connection(**cinfo) as sftp:
        print("Uploading "+JARFILE)
        sftp.put(JARFILE)  


deleteOldJars()
uploadJar()